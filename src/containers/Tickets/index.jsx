import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { ticketsContainerSelector } from '../../selectors';
import { fetchTickets } from '../../actions/ticketsActions';
import { changeFilters } from '../../actions/filtersActions';
import Ticket from '../../components/Ticket';
import Filters from '../../components/Filters';
import Sorter from '../../components/Sorter';
import { getValidFilters } from '../../utils';
import Loader from '../Loader';
import styles from './styles.scss';


@connect(ticketsContainerSelector, dispatch => ({
  actions: bindActionCreators({ fetchTickets, changeFilters, push }, dispatch),
}))
export default class Tickets extends React.Component {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    actions: PropTypes.object.isRequired,
    minDuration: PropTypes.number,
    maxDuration: PropTypes.number,
    items: PropTypes.array,
    filters: PropTypes.object,
    availableDates: PropTypes.array,
  };

  static defaultProps = {
    filters: {},
    items: [],
    availableDates: [],
    minDuration: 1,
    maxDuration: 20,
  };

  componentDidMount() {
    this.props.actions.fetchTickets();
  }

  handleFiltersChange = (data) => {
    const { actions, filters } = this.props;
    const nextFilters = getValidFilters({ ...filters, ...data });
    actions.push(`/?${queryString.stringify(nextFilters)}`);
  };

  render() {
    const {
      items,
      loading,
      availableDates,
      filters,
      minDuration,
      maxDuration,
    } = this.props;

    return (
      <Loader loading={loading}>
        <div className={styles.container}>
          <Filters
            minDuration={minDuration}
            maxDuration={maxDuration}
            onChange={this.handleFiltersChange}
            availableDates={availableDates}
            filters={filters}
          />

          <div className={styles.inner}>
            <Sorter
              onChange={this.handleFiltersChange}
              sortBy={filters.sortBy}
            />

            <div className={styles.ticketsList}>
              {items.map(ticket => (
                <Ticket key={`ticket-${ticket.id}`} data={ticket} />
              ))}
            </div>
          </div>
        </div>
      </Loader>
    );
  }
}
