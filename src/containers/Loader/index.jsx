import React from 'react';
import PropTypes from 'prop-types';
import Loading from '../../components/Loading';

export default function Loader({ loading, children }) {
  return loading ? <Loading /> : children;
}

Loader.propTypes = {
  loading: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
};

