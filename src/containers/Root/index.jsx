import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Route } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';
import Tickets from '../Tickets';
import styles from './styles.scss';


const Root = ({ store, history }) => (
  <div className={styles.root}>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Route path="/" component={Tickets} />
      </ConnectedRouter>
    </Provider>
  </div>
);

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default Root;
