import { all, fork } from 'redux-saga/effects';
import tickets from './ticketsSaga';
import filters from './filtersSaga';

export default function* root() {
  yield all([
    fork(filters),
    fork(tickets),
  ]);
}
