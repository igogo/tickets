import { LOCATION_CHANGE } from 'react-router-redux';
import { put, select, takeLatest } from 'redux-saga/effects';
import queryString from 'query-string';
import { actionTypes } from '../reducers/filtersReducer';
import { filtersSelector } from '../selectors';


export default function* () {
  yield takeLatest(LOCATION_CHANGE, function* locationChange({ payload }) {
    const filters = yield select(filtersSelector);

    yield put({
      type: actionTypes.CHANGE,
      payload: {
        ...filters,
        ...queryString.parse(payload.search),
      },
    });
  });
}
