import { put, takeLatest } from 'redux-saga/effects';
import { actionTypes } from '../reducers/ticketsReducer';

export default function* () {
  yield takeLatest(actionTypes.FETCH, function* fetchTickets() {
    try {
      const data = yield fetch('https://api.myjson.com/bins/oivjj');
      const tickets = yield data.json();
      yield put({ type: actionTypes.FETCH_SUCCESS, payload: tickets });
    } catch (e) {
      yield put({ type: actionTypes.FETCH_FAILED, payload: e.message });
    }
  });
}
