import React from 'react';
import ReactDOM from 'react-dom';
import Root from './containers/Root';
import configureStore from './config/configureStore';

const { store, history } = configureStore();

ReactDOM.render(
  <Root history={history} store={store} />,
  document.getElementById('root'),
);
