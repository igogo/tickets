import { createSelector } from 'reselect';
import { getMonthAndYear, getMonthAndDay, getMonth } from '../utils';

const getFilterDates = (items) => {
  const availableDates = Object.values(items.reduce((dates, item) => {
    // Dates only with "availability"
    item.dates.filter(date => date.availability > 0).forEach((date) => {
      const readableMonth = getMonthAndYear(date.start);
      const month = getMonth(date.start);
      if (!dates[month]) {
        dates[month] = {
          month,
          date: readableMonth,
          count: 0,
        };
      }

      dates[month].count++;
    });

    return dates;
  }, {}))
    .filter(item => item.count > 10);

  availableDates.sort((a, b) => a.time - b.time);

  return availableDates;
};

const getCheapestDates = (dates, filters) => {
  const cheapestDates = [].concat(dates).sort((a, b) => (
    a.usd < b.usd &&
    a.availability > b.availability
  ));

  // Get two cheapest dates
  return cheapestDates.filter(date => !(
    (filters.fullDate && date.start !== filters.fullDate) ||
    (filters.date && !date.start.startsWith(filters.date))
  )).slice(0, 2).map(date => ({
    ...date,
    startReadable: getMonthAndDay(date.start),
  }));
};


const getDurations = (items) => {
  const itemsDurations = items.map(item => item.length);
  return {
    minDuration: Math.min(...itemsDurations),
    maxDuration: Math.max(...itemsDurations),
  };
};

const normalizeItem = ({
  id,
  url,
  name,
  length,
  images,
  rating,
  cities,
  reviews,
  description,
  dates,
  length_type: lengthType,
  operator_name: operatorName,
}, filters) => {
  const cheapestDates = getCheapestDates(dates, filters);
  const firstDate = cheapestDates.length > 0 ? cheapestDates[0] : {};

  return {
    id,
    url,
    name,
    length,
    images,
    rating,
    reviews,
    lengthType,
    description,
    operatorName,
    dates: cheapestDates,
    price: firstDate.usd,
    discount: firstDate.discount ? firstDate.discount : '',
    startCity: cities[0].name,
    destinations: cities.length,
    endCity: cities[cities.length - 1].name,
  };
};

const filterItem = (item, {
  fullDate,
  date,
  durationFrom,
  durationTo,
}) => {
  if (!fullDate && !date && !durationFrom && !durationTo) {
    return true;
  }

  const isValidDates = (
    (!fullDate && !date) ||
    item.dates.some(dt => (
      (fullDate && dt.start === fullDate) ||
      (date && dt.start.startsWith(date))
    ))
  );

  if (!isValidDates) {
    return false;
  }

  const isValidDuration = (
    (!durationFrom && !durationTo) ||
    (item.length >= durationFrom && item.length <= durationTo)
  );

  if (!isValidDuration) {
    return false;
  }

  return isValidDates && isValidDuration;
};

const getFilteredItems = (items, filters) => (
  [].concat(items)
    .filter(item => filterItem(item, filters))
    .map(item => normalizeItem(item, filters))
    .sort((a, b) => {
      switch (filters.sortBy) {
        case 'price': {
          return a.price > b.price;
        }
        case 'rating':
        default: {
          return a.rating < b.rating;
        }
      }
    })
);

export const ticketsSelector = ({ tickets }) => tickets;

export const filtersSelector = ({ filters }) => filters;

export const ticketsContainerSelector = createSelector(
  ticketsSelector,
  filtersSelector,
  ({ items, loading }, filters) => ({
    filters,
    loading,
    items: getFilteredItems(items, filters),
    availableDates: getFilterDates(items),
    ...getDurations(items),
  }),
);
