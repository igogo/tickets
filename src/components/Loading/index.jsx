import React from 'react';
import styles from './styles.scss';

const Loading = () => (
  <div className={styles.loading}>
    Loading...
  </div>
);

export default Loading;
