import React from 'react';
import PropTypes from 'prop-types';
import InputRange from 'react-input-range';
import { throttle } from 'throttle-debounce';
import styles from './styles.scss';

const FilterRange = ({
  min,
  max,
  durationFrom,
  durationTo,
  handleChange,
}) => (
  <div className={styles.filterRange} onChange={handleChange}>
    <div className={styles.title}>
      Duration
    </div>

    <InputRange
      minValue={min}
      maxValue={max}
      formatLabel={value => `${value} days`}
      value={{
        min: durationFrom,
        max: durationTo,
      }}
      onChange={throttle(500, value => handleChange({
        durationFrom: value.min,
        durationTo: value.max,
      }))}
    />

  </div>
);

FilterRange.propTypes = {
  durationFrom: PropTypes.number,
  durationTo: PropTypes.number,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  handleChange: PropTypes.func.isRequired,
};

FilterRange.defaultProps = {
  durationFrom: null,
  durationTo: null,
};

export default FilterRange;
