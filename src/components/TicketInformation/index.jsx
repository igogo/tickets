import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scss';

const TicketInformation = ({
  name,
  length,
  description,
  destinations,
  lengthType,
  startCity,
  endCity,
  operatorName,
}) => (
  <div className={styles.informationContainer}>
    <h2 className={styles.title}>{name} - {length} {lengthType}</h2>
    <p title={description} className={styles.description}>{description}</p>

    <table className={styles.infoTable}>
      <tbody>
        <tr>
          <th>{lengthType}</th>
          <td>{length} {lengthType.toLowerCase()}</td>
        </tr>
        <tr>
          <th>Destination</th>
          <td>{destinations} cities</td>
        </tr>
        <tr>
          <th>Starts/Ends</th>
          <td>{startCity} / {endCity}</td>
        </tr>
        <tr>
          <th>Operator</th>
          <td>{operatorName}</td>
        </tr>
      </tbody>
    </table>
  </div>
);

TicketInformation.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  length: PropTypes.number.isRequired,
  destinations: PropTypes.number.isRequired,
  startCity: PropTypes.string.isRequired,
  endCity: PropTypes.string.isRequired,
  operatorName: PropTypes.string.isRequired,
  lengthType: PropTypes.string.isRequired,
};

export default TicketInformation;
