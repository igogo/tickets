import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import priceIcon from './price-icon.png';
import styles from './styles.scss';

const TicketPrice = ({ price, dates, url }) => (
  <div className={styles.priceContainer}>
    <p className={styles.priceTitle}>Total price</p>
    <p className={styles.price}>
      ${price}
      <img className={styles.priceIcon} src={priceIcon} alt="Price" />
    </p>

    <div className={styles.devider} />

    <table className={styles.datesTable}>
      <tbody>
        {dates.map(date => (
          <tr key={`${date.start}-${date.availability}`}>
            <td data-value={date.start}>{date.startReadable}</td>
            <td
              className={classNames(styles.availability, {
                low: date.availability <= 3,
              })}
            >
              {date.availability} seats left
            </td>
          </tr>
        ))}
      </tbody>
    </table>

    <div className={styles.devider} />

    <a
      href={url}
      className={styles.viewMoreBtn}
      target="_blank"
    >
      View more
    </a>
  </div>
);

TicketPrice.propTypes = {
  price: PropTypes.number.isRequired,
  url: PropTypes.string.isRequired,
  dates: PropTypes.array.isRequired,
};

export default TicketPrice;
