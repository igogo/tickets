import React from 'react';
import PropTypes from 'prop-types';
import FilterDate from '../FilterDate';
import FilterRange from '../FilterRange';
import styles from './styles.scss';

const Filters = ({
  minDuration,
  maxDuration,
  filters,
  availableDates,
  onChange,
}) => (
  <div className={styles.filtersWrapper}>
    <div className={styles.title}>Filter by</div>

    <FilterDate
      selectedDate={filters.date}
      fullDate={filters.fullDate}
      availableDates={availableDates}
      handleChange={onChange}
    />

    <FilterRange
      min={minDuration}
      max={maxDuration}
      durationFrom={parseInt(filters.durationFrom, 10) || minDuration}
      durationTo={parseInt(filters.durationTo, 10) || maxDuration}
      handleChange={onChange}
    />
  </div>
);

Filters.propTypes = {
  filters: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  availableDates: PropTypes.array.isRequired,
  minDuration: PropTypes.number.isRequired,
  maxDuration: PropTypes.number.isRequired,
};

export default Filters;
