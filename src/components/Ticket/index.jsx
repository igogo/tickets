import React from 'react';
import PropTypes from 'prop-types';
import TicketImage from '../TicketImage';
import TicketInformation from '../TicketInformation';
import TicketPrice from '../TicketPrice';
import styles from './styles.scss';

const lengthTypes = {
  d: 'Days',
  w: 'Weeks',
  m: 'Months',
};

const Ticket = ({
  data: {
    rating,
    reviews,
    images,
    name,
    description,
    days,
    destinations,
    startCity,
    endCity,
    operatorName,
    length,
    lengthType,
    url,
    dates,
    price,
    discount,
  },
}) => (
  <div className={styles.ticket}>
    <TicketImage
      rating={rating}
      reviews={reviews}
      images={images}
      name={name}
      discount={discount}
    />

    <div className={styles.dataContainer}>
      <TicketInformation
        name={name}
        description={description}
        days={days}
        destinations={destinations}
        startCity={startCity}
        endCity={endCity}
        operatorName={operatorName}
        length={length}
        lengthType={lengthTypes[lengthType]}
      />

      <TicketPrice
        url={url}
        dates={dates}
        price={price}
      />
    </div>
  </div>
);

Ticket.propTypes = {
  data: PropTypes.object.isRequired,
};

export default Ticket;
