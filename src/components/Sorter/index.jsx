import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scss';

const sortings = [
  {
    value: 'rating',
    label: 'Popularity',
  },
  {
    value: 'price',
    label: 'Price',
  },
];

const controlId = 'sorter';

const Sorter = ({ onChange, sortBy }) => (
  <div className={styles.sorterWrapper}>
    <label htmlFor={controlId} className={styles.label}>
      Sort by
      <select
        className={styles.select}
        onChange={e => onChange({ sortBy: e.target.value })}
        value={sortBy}
        id={controlId}
      >
        {sortings.map(opt => (
          <option value={opt.value} key={opt.value}>{opt.label}</option>
        ))}
      </select>
    </label>
  </div>
);

Sorter.propTypes = {
  onChange: PropTypes.func.isRequired,
  sortBy: PropTypes.oneOf(sortings.map(opt => opt.value)).isRequired,
};

export default Sorter;
