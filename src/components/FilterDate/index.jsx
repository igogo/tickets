import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import styles from './styles.scss';

const DATE_FORMAT = 'YYYY-MM-DD';

const FilterDate = ({
  availableDates,
  selectedDate,
  fullDate,
  handleChange,
}) => (
  <div className={styles.filterDate} onChange={handleChange}>
    <div className={styles.title}>
      Departure date
    </div>

    <div className={styles.datesList}>
      {availableDates.map(date => (
        <button
          key={date.date}
          className={classNames(styles.availableDate, {
            [styles.selectedDate]: date.month === selectedDate,
          })}
          onClick={() => handleChange({ date: date.month, fullDate: null })}
        >
          {date.date} ({date.count})
        </button>
      ))}
    </div>

    <DatePicker
      placeholderText="Select a specific date"
      className={styles.datePicker}
      selected={fullDate && moment(fullDate)}
      onChange={date => handleChange({ fullDate: moment(date).format(DATE_FORMAT) })}
      dateFormat={DATE_FORMAT}
    />
  </div>
);

FilterDate.propTypes = {
  selectedDate: PropTypes.string,
  fullDate: PropTypes.string,
  availableDates: PropTypes.array,
  handleChange: PropTypes.func.isRequired,
};

FilterDate.defaultProps = {
  fullDate: null,
  selectedDate: null,
  availableDates: {},
};

export default FilterDate;
