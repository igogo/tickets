import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './styles.scss';

export default class LazyImg extends React.Component {
  static propTypes = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoaded: false,
    };
  }

  componentDidMount() {
    this.img.setAttribute('src', this.props.src);
    this.img.onload = () => {
      this.setState({ isLoaded: true });
    };
  }

  render() {
    return (
      <img
        ref={(img) => { this.img = img; }}
        className={classNames(styles.lazyImg, {
          [styles.loaded]: this.state.isLoaded,
        })}
        alt={this.props.alt}
      />
    );
  }
}
