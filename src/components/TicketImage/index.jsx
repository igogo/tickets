import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scss';
import heartImg from './HEART.svg';
import halfStarImg from './HALF.png';
import starImg from './FULL.svg';
import LazyImg from '../LazyImg';

const getImage = (images) => {
  if (!images || !Array.isArray(images)) {
    return null;
  }

  const primaryImage = images.find(image => image.is_primary);
  if (primaryImage) {
    return primaryImage.url;
  }

  if (images[0]) {
    return images[0].url;
  }

  return null;
};

const renderRating = (rating) => {
  const stars = [];
  for (let i = rating; i > 0; i--) {
    stars.push(<img key={`star-${i}`} src={i < 1 ? halfStarImg : starImg} alt="Star" />);
  }

  return stars;
};

const TicketImage = ({
  rating,
  reviews,
  images,
  name,
  discount,
}) => (
  <div className={styles.imageContainer}>
    <div className={styles.inner}>
      <LazyImg src={getImage(images)} alt={name} />
      <img className={styles.hearth} src={heartImg} alt="Heart" />

      {discount && (
        <div className={styles.discountTriangle}>
          <span className={styles.discount}>-{discount}</span>
        </div>
      )}

      <div className={styles.starsContainer}>
        {renderRating(rating)}
      </div>

      <div className={styles.reviews}>
        {reviews} reviews
      </div>
    </div>
  </div>
);

TicketImage.propTypes = {
  rating: PropTypes.number.isRequired,
  reviews: PropTypes.number.isRequired,
  images: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  discount: PropTypes.string.isRequired,
};

export default TicketImage;
