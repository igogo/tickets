// @flow
import Immutable from 'seamless-immutable';
import { createReducer } from '../utils';

export const actionTypes = {
  CHANGE: 'FILTERS/CHANGE',
};

export const DEFAULT_SORTING = 'rating';

export type filtersType = {
  date: ?string,
  fullDate: ?string,
  durationFrom: ?string,
  durationTo: ?string,
  sortBy: string,
};

const initialState: filtersType = new Immutable({
  date: null,
  fullDate: null,
  durationFrom: null,
  durationTo: null,
  sortBy: DEFAULT_SORTING,
});

export default createReducer(initialState, {
  [actionTypes.CHANGE]: (state: filtersType, payload): filtersType => state.merge(payload),
});
