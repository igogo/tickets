// @flow
import Immutable from 'seamless-immutable';
import { createReducer } from '../utils';

export const actionTypes = {
  FETCH: 'TICKETS/FETCH',
  FETCH_SUCCESS: 'TICKETS/FETCH_SUCCESS',
  FETCH_FAILED: 'TICKETS/FETCH_FAILED',
};

export type ticketType = {
  id: number,
  code: string,
  description: string,
  length: number,
  length_type: string,
  name: string,
  updated: string,
  website: string,
  operator_name: string,
  cities: [{
    name: string,
    lng: number,
    lat: number
  }],
  dates: [{}],
  images: [{
    name: string,
    created_at: number,
    is_primary: boolean,
    url: string,
  }],
  active: boolean,
  url: string,
  rating: number,
  reviews: number
};

export type ticketsType = {
  loading: boolean,
  items: Array<ticketType>,
};

const initialState: ticketsType = new Immutable({
  loading: true,
  items: [],
});

export default createReducer(initialState, {
  [actionTypes.FETCH]: (state: ticketsType): ticketsType => (
    state.set('items', []).set('loading', true)
  ),

  [actionTypes.FETCH_FAILED]: (state: ticketsType): ticketsType => (
    state.set('items', []).set('loading', false)
  ),

  [actionTypes.FETCH_SUCCESS]: (state: ticketsType, items): ticketsType => (
    state.set('items', items).set('loading', false)
  ),
});
