import { routerReducer as routing } from 'react-router-redux';
import tickets from './ticketsReducer';
import filters from './filtersReducer';


export default {
  routing,
  tickets,
  filters,
};
