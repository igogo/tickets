import { actionTypes } from '../reducers/filtersReducer';

export const changeFilters = nextFilters => ({
  type: actionTypes.CHANGE,
  payload: nextFilters,
});
