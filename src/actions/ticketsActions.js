import { actionTypes } from '../reducers/ticketsReducer';

export const fetchTickets = () => ({
  type: actionTypes.FETCH,
});
