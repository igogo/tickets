import moment from 'moment/moment';

export const createReducer = (initialState, reducerMap) => (state = initialState, action) => {
  if (typeof reducerMap[action.type] === 'function') {
    return reducerMap[action.type](state, action.payload);
  }
  return state;
};

export const getValidFilters = (filters) => {
  if (filters.fullDate) {
    filters.date = null;
  }

  return filters;
};

export const getMonth = date => moment(date).format('YYYY-MM');

export const getMonthAndYear = date => moment(date).format('MMM YYYY');

export const getMonthAndDay = date => moment(date).format('DD MMM');
