import { createStore, combineReducers, applyMiddleware } from 'redux';
import createHistory from 'history/createBrowserHistory';
import logger from 'redux-logger';
import sagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import sagas from '../sagas';
import reducers from '../reducers';


export default () => {
  const saga = sagaMiddleware();

  const history = createHistory();

  const store = createStore(
    combineReducers(reducers),
    applyMiddleware(routerMiddleware(history), saga, logger),
  );

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers'); // eslint-disable-line global-require
      store.replaceReducer(nextRootReducer);
    });
  }

  saga.run(sagas);

  return { store, history };
};
