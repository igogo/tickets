const webpack = require('webpack');
const path = require('path');

const isDevelopment = process.env.NODE_ENV === 'development';
const plugins = [];

if (!isDevelopment) {
  plugins.push(new webpack.optimize.UglifyJsPlugin());
}

const config = {
  bail: true,
  cache: false,
  devtool: isDevelopment && 'eval',

  entry: [
    'regenerator-runtime/runtime',
    './src/index.jsx',
  ],

  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
  },

  module: {
    rules: [
      {
        test: /\.(jsx?)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
            },
          },
        ],
      },
      {
        test: /\.(jsx?)$/,
        enforce: 'pre',
        exclude: /node_modules/,
        use: ['eslint-loader'],
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
          'sprite-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpg|svg)$/,
        use: [
          'file-loader?name=[path][name].[ext]',
        ],
      },
    ],
  },

  resolve: {
    extensions: ['.js', '.jsx', '.scss', '.png', '.svg', '.png'],
  },

  devServer: {
    historyApiFallback: {
      rewrites: [
        { from: /.*/, to: '/' },
      ],
    },
  },

  plugins,
};

module.exports = config;
